package isac.cloud;

import java.net.*;
import java.io.*;

public class TestService {

	public static void main(String[] args) throws Exception {
		try {
			URL url = new URL("http://localhost:8888/isac1516test");
			//URL url = new URL("http://projecttest-965.appspot.com/demo");
			 
			HttpURLConnection con = (HttpURLConnection) url.openConnection();	 
			con.setRequestMethod("PUT");
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.close();
	 
			int responseCode = con.getResponseCode();
			System.out.println("POST to " + url);
			System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
			System.out.println(response.toString());
		} catch (Exception ex){
			ex.printStackTrace();
		} finally {

		}

	}
}
