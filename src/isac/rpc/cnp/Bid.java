package isac.rpc.cnp;

import java.io.Serializable;

public class Bid implements Serializable {
	
	private final ContractorId id;
	private final ContractorNotifications ref;
	private final int bid;

	public Bid(ContractorId id, ContractorNotifications ref, int bid) {
		super();
		this.id = id;
		this.ref = ref;
		this.bid = bid;
	}

	public ContractorId getId() {
		return id;
	}

	public ContractorNotifications getRef() {
		return ref;
	}

	public int getBid() {
		return bid;
	}
	
	

}
