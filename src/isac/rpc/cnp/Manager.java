package isac.rpc.cnp;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class Manager extends Thread  {

	private ManagerCNP board;
	
	public Manager() {
	}
	
	public void run(){
		try {
			log("Setting up the CNP Board...");
			setupCNPBoard();
			
			log("CNP Board ready.");
			waitFor(5000);
			
			log("Advertising a new task");
			Task t = new Task();
			board.advertise(t);
			
			log("Waiting for contractors' bids for 5 secs");
			waitFor(5000);
			
			log("Evaluating bids...");
			Optional<Bid> awarded = selectAwarded();			
			if (awarded.isPresent()){
				Bid winner = awarded.get();
				log("Awarded contractor: "+winner.getId());
				winner.getRef().notifyAwarded();
				for (Bid b: board.getBids()){
					if (b.getId() != winner.getId()){
						b.getRef().notifyNotAwarded();
					}
				}
			} else {
				log("No contractor awarded.");
			}
			log("quit.");
			
		} catch (RemoteException ex){
			ex.printStackTrace();
		}
	}
	
	protected void setupCNPBoard() throws RemoteException {
		board = new ManagerCNP();
		ContractorActions stub = (ContractorActions) UnicastRemoteObject.exportObject(board, 0);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("CNP-Board", stub);
	}
	
	protected Optional<Bid> selectAwarded(){
		List<Bid> bids = board.getBids();
		Optional<Bid> aw = Optional.empty();
		int min = Integer.MAX_VALUE;
		for (Bid b: bids){
			if (b.getBid() <  min){
				min = b.getBid();
				aw = Optional.of(b);
			}
		}
		return aw;
	}
	
	protected void waitFor(int ms){
		try {
			sleep(ms);
		} catch (Exception ex){}
	}
	
	protected void log(String msg){
		synchronized (System.out){
			System.out.println("[MANAGER] "+msg);
		}
	}

}
