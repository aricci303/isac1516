package isac.rpc.cnp;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ManagerCNP implements ContractorActions, ManagerActions {

	private List<ContractorNotifications> contractors;
	private List<Bid> bids;

	public ManagerCNP(){
		contractors = new ArrayList<ContractorNotifications>();
		bids = new LinkedList<Bid>();
	}
	
	@Override
	public synchronized void advertise(Task task) {
		for (ContractorNotifications c: contractors){
			try {
				c.notifyNewTask(task);
			} catch (RemoteException ex){
				/* WHAT TO DO ? */
				ex.printStackTrace();
			}
		}		
	}

	@Override
	public synchronized void bid(Bid bid) throws RemoteException {
		bids.add(bid);
	}

	@Override
	public synchronized void register(ContractorNotifications c) throws RemoteException {
		contractors.add(c);
	}

	@Override
	public synchronized List<Bid> getBids() {
		return bids;
	}

}
