package isac.rpc;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Client {

    private Client() {}

    public static void main(String[] args) {

        String host = (args.length < 1) ? null : args[0];
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            HelloService stub = (HelloService) registry.lookup("Hello");
            
            MyClass obj = new MyClass(300); 
            UnicastRemoteObject.exportObject(obj, 0);
            
            String response = stub.sayHello(obj);
            
            System.out.println("response: " + response);
            
            System.out.println(">> "+obj.get());
            
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}